set nocompatible              " be iMproved, required
filetype off                  " required
filetype plugin indent on    " required
syntax enable
set tabstop=4
set softtabstop=4
set expandtab
set nobackup
set mouse=c
set backspace=2
set background=dark
set shiftwidth=4
set linebreak
set number
set autoindent
set showcmd
set textwidth=0
""set cursorline
set wildmenu
set lazyredraw
set showmatch
set incsearch
set hlsearch
set ignorecase
set autochdir
set hidden
set laststatus=2
set tags=./tags;/
set splitbelow
set splitright
set belloff=all
nnoremap <leader><space> :nohlsearch<CR>
nnoremap <leader>g :vimgrep // % <CR> :copen<CR>
nnoremap <leader>f :g//z#.5 <CR> :copen<CR>
nnoremap j gj
nnoremap k gk
nnoremap <C-j> <C-w><C-J>
nnoremap <C-k> <C-w><C-k>
nnoremap <C-l> <C-w><C-l>
nnoremap <C-h> <C-w><C-h>
nnoremap <Leader>j <C-w>10-
nnoremap <Leader>k <C-w>10+
nnoremap <Leader>s :syntax sync fromstart<CR>
nnoremap <space> za
au filetype html inoremap </ </<C-X><C-O>

"Curly Brace
inoremap { {}<Left>
inoremap <expr> } strpart(getline('.'), col('.')-1, 1) == "}" ? "\<Right>" : "}"

"Brackets
inoremap [ []<Left>
inoremap <expr> ] strpart(getline('.'), col('.')-1, 1) == "]" ? "\<Right>" : "]"

"Parenthesis
inoremap ( ()<Left>
inoremap <expr> ) strpart(getline('.'), col('.')-1, 1) == ")" ? "\<Right>" : ")"

"Quotes
"inoremap <expr> ' strpart(getline('.'), col('.')-1, 1) == "'" ? "\<Right>" : "\'\'\<Left>"
"inoremap <expr> " strpart(getline('.'), col('.')-1, 1) == "\"" ? "\<Right>" : "\"\"\<Left>"

function! SingleQuote()
	if strpart(getline('.'), col('.')-1, 1) == "'" && strpart(getline('.'), col('.')-2, 1) == "'"
		return "\<Right>\<BS>"
    elseif strpart(getline('.'), col('.')-1, 1) == "'"
        return "\<Right>"
    else
        return "\'\'\<Left>"
endfunction

function! DoubleQuote()
	if strpart(getline('.'), col('.')-1, 1) == "\"" && strpart(getline('.'), col('.')-2, 1) == "\""
		return "\<Right>\<BS>"
    elseif strpart(getline('.'), col('.')-1, 1) == "\""
        return "\<Right>"
    else
        return "\"\"\<Left>"
endfunction

""inoremap <expr> ' SingleQuote()
""inoremap <expr> " DoubleQuote()
"Function to make backspace work really nice for the brackets, parenthesis etc
function! BackSpace()
	"If the character before and after is the same delete them both aka (|) -> |

	" Double Quotes
	if strpart(getline('.'), col('.')-1, 1) == "\"" && strpart(getline('.'), col('.')-2, 1) == "\""
		return "\<Right>\<BS>\<BS>"

	" Single Quotes
	elseif strpart(getline('.'), col('.')-1, 1) == "'" && strpart(getline('.'), col('.')-2, 1) == "'"
		return "\<Right>\<BS>\<BS>"

	" Curly Brace
	elseif strpart(getline('.'), col('.')-1, 1) == "}" && strpart(getline('.'), col('.')-2, 1) == "{"
		return "\<Right>\<BS>\<BS>"

	" Brackets
	elseif strpart(getline('.'), col('.')-1, 1) == "]" && strpart(getline('.'), col('.')-2, 1) == "["
		return "\<Right>\<BS>\<BS>"

	" Parenthesis
	elseif strpart(getline('.'), col('.')-1, 1) == ")" && strpart(getline('.'), col('.')-2, 1) == "("
		return "\<Right>\<BS>\<BS>"

	else
		return "\<BS>"
	endif
endfunction

function! Enter()
	"If the character before and after is a curly, parenthesis, or bracket
	"do the cool spacing parts for it
	" Double Quotes
	" Curly Brace
	if strpart(getline('.'), col('.')-1, 1) == "}" && strpart(getline('.'), col('.')-2, 1) == "{"
		return "\<CR>\<ESC>O"

	" Brackets
	elseif strpart(getline('.'), col('.')-1, 1) == "]" && strpart(getline('.'), col('.')-2, 1) == "["
		return "\<CR>\<ESC>O"

	" Parenthesis
	elseif strpart(getline('.'), col('.')-1, 1) == ")" && strpart(getline('.'), col('.')-2, 1) == "("
		return "\<CR>\<ESC>O"

	else
		return "\<CR>"
	endif
endfunction	

inoremap <expr> <BS> BackSpace()
inoremap <expr> <CR> Enter()

"Helps with lists in markdown
function! BetterBullets()
	set formatoptions+=ro
	set breakindent
	set comments=b:*,b:-,b:+,n:>,b:1.
    set breakindent
    set breakindentopt=shift:2
    set foldmethod=syntax
    autocmd BufEnter * :syntax sync fromstart
    syn match Title /.*$/ contained
    syn match Delimiter /^#\+\s/ contained nextgroup=Title
    syn region level1 start=/^#\s.*/ end=/^#\s.*/re=s-1,me=s-1 keepend contains=CONTAINED,Title fold
    syn region level2 start=/^##\s.*/ end=/^##\s.*/re=s-1,me=s-1 keepend contained contains=CONTAINED,Title fold
    syn region level3 start=/^###\s.*/ end=/^###\s.*/re=s-1,me=s-1 keepend contained contains=CONTAINED,Title fold
    syn match Comment "//\s.*" contained
    syn match Statement "<.\{-}>" contained
    syn match String /".\{-}"/ contained
endfunction
autocmd Filetype markdown call BetterBullets()
autocmd Filetype text call BetterBullets()
